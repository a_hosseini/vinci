package mklib.hosseini.com.vinci.Callbacks;

/**
 * Created by abbas on 2/9/16.
 */
public interface ResultProcess {

    void onFinish(byte[] output);

}
